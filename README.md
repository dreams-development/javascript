# Javascript
## Code guides
- [Airbnb JavaScript Style Guide](https://github.com/airbnb/javascript)
- [ES6 Best practises](https://github.com/DrkSephy/es6-cheatsheet)
- [Cheatsheets](https://devhints.io/)

## Articles
### How javascript works
- [An overview of the engine, the runtime, and the call stack](https://blog.sessionstack.com/how-does-javascript-actually-work-part-1-b0bacc073cf), [[RUS]](https://habr.com/company/ruvds/blog/337042/)
- [Inside the V8 engine + 5 tips on how to write optimized code](https://blog.sessionstack.com/how-javascript-works-inside-the-v8-engine-5-tips-on-how-to-write-optimized-code-ac089e62b12e), [[RUS]](https://habr.com/company/ruvds/blog/337460/)
- [Memory management + how to handle 4 common memory leaks](https://blog.sessionstack.com/how-javascript-works-memory-management-how-to-handle-4-common-memory-leaks-3f28b94cfbec), [[RUS]](https://habr.com/company/ruvds/blog/338150/)
- [Event loop and the rise of Async programming + 5 ways to better coding with async/await](https://blog.sessionstack.com/how-javascript-works-event-loop-and-the-rise-of-async-programming-5-ways-to-better-coding-with-2f077c4438b5), [[RUS]](https://habr.com/company/ruvds/blog/340508/)
- [Deep dive into WebSockets and HTTP/2 with SSE + how to pick the right path](https://blog.sessionstack.com/how-javascript-works-deep-dive-into-websockets-and-http-2-with-sse-how-to-pick-the-right-path-584e6b8e3bf7), [[RUS]](https://habr.com/company/ruvds/blog/342346/)
- [A comparison with WebAssembly + why in certain cases it’s better to use it over JavaScript](https://blog.sessionstack.com/how-javascript-works-a-comparison-with-webassembly-why-in-certain-cases-its-better-to-use-it-d80945172d79), [[RUS]](https://habr.com/company/ruvds/blog/343568/)
- [The building blocks of Web Workers + 5 cases when you should use them](https://blog.sessionstack.com/how-javascript-works-the-building-blocks-of-web-workers-5-cases-when-you-should-use-them-a547c0757f6a), [[RUS]](https://habr.com/company/ruvds/blog/348424/)
- [Service Workers, their lifecycle and use cases](https://blog.sessionstack.com/how-javascript-works-service-workers-their-life-cycle-and-use-cases-52b19ad98b58), [[RUS]](https://habr.com/company/ruvds/blog/349858/)
- [How JavaScript works: the mechanics of Web Push Notifications](https://blog.sessionstack.com/how-javascript-works-the-mechanics-of-web-push-notifications-290176c5c55d), [[RUS]](https://habr.com/company/ruvds/blog/350486/)
- [Tracking changes in the DOM using MutationObserver](https://blog.sessionstack.com/how-javascript-works-tracking-changes-in-the-dom-using-mutationobserver-86adc7446401). [[RUS]](https://habr.com/company/ruvds/blog/351256/)
- [The rendering engine and tips to optimize its performance](https://blog.sessionstack.com/how-javascript-works-the-rendering-engine-and-tips-to-optimize-its-performance-7b95553baeda)[[RUS]](https://habr.com/company/ruvds/blog/351802/)
- [Inside the Networking Layer + How to Optimize Its Performance and Security](https://blog.sessionstack.com/how-javascript-works-inside-the-networking-layer-how-to-optimize-its-performance-and-security-f71b7414d34c)[[RUS]](https://habr.com/company/ruvds/blog/354070/)
- [Under the hood of CSS and JS animations + how to optimize their performance](https://blog.sessionstack.com/how-javascript-works-under-the-hood-of-css-and-js-animations-how-to-optimize-their-performance-db0e79586216) [[RUS]](https://habr.com/company/ruvds/blog/354438/)
- [How JavaScript works: Parsing, Abstract Syntax Trees (ASTs) + 5 tips on how to minimize parse time](https://blog.sessionstack.com/how-javascript-works-parsing-abstract-syntax-trees-asts-5-tips-on-how-to-minimize-parse-time-abfcf7e8a0c8) [[RUS]](https://habr.com/company/ruvds/blog/415269/)
- [How JavaScript works: The internals of classes and inheritance + transpiling in Babel and TypeScript](https://blog.sessionstack.com/how-javascript-works-the-internals-of-classes-and-inheritance-transpiling-in-babel-and-113612cdc220) [[RUS]](https://habr.com/company/ruvds/blog/415377/)
- [How JavaScript works: Storage engines + how to choose the proper storage API](https://blog.sessionstack.com/how-javascript-works-the-internals-of-shadow-dom-how-to-build-self-contained-components-244331c4de6e) [[RUS]](https://habr.com/company/ruvds/blog/415505/)
- [How JavaScript works: the internals of Shadow DOM + how to build self-contained components](https://blog.sessionstack.com/how-javascript-works-the-internals-of-shadow-dom-how-to-build-self-contained-components-244331c4de6e) [[RUS]](https://habr.com/company/ruvds/blog/415881/)
- [How JavaScript works: WebRTC and the mechanics of peer to peer networking](https://blog.sessionstack.com/how-javascript-works-webrtc-and-the-mechanics-of-peer-to-peer-connectivity-87cc56c1d0ab) [[RUS]](https://habr.com/company/ruvds/blog/416821/)
- [How JavaScript works: Under the hood of custom elements + Best practices on building reusable components](https://blog.sessionstack.com/how-javascript-works-under-the-hood-of-custom-elements-best-practices-on-building-reusable-e118e888de0c)[[RUS]](https://habr.com/company/ruvds/blog/419831/)

### Common
- [JavaScript VM internals, EventLoop, Async and ScopeChains](https://www.youtube.com/watch?v=QyUFheng6J0&feature=youtu.be)
- [Jake Archibald: все что я знаю про Event Loop в JavaScript (2018)](https://www.youtube.com/watch?v=j4_9BZezSUA&index=4&t=0s&list=WL)
- [JavaScript type coercion explained](https://medium.freecodecamp.org/js-type-coercion-explained-27ba3d9a2839), [[RUS]](https://habr.com/company/ruvds/blog/347866/)
- [S.O.L.I.D The first 5 principles of Object Oriented Design with JavaScript](https://medium.com/@cramirez92/s-o-l-i-d-the-first-5-priciples-of-object-oriented-design-with-javascript-790f6ac9b9fa)
- [S.O.L.I.D-ый JavaScript](https://www.youtube.com/watch?v=wi3wPzReKZQ&t=8s)
- [An Introduction to Regular Expressions (Regex) In JavaScript](https://codeburst.io/an-introduction-to-regular-expressions-regex-in-javascript-1d3559e7ac9a)
- [Using web workers for safe, concurrent JavaScript](https://blog.logrocket.com/using-webworkers-for-safe-concurrent-javascript-3f33da4eb0b2), [[RUS]](https://habr.com/company/ruvds/blog/352828/)
- [Работа с буфером обмена в JavaScript с использованием асинхронного API Clipboard](https://habr.com/company/ruvds/blog/358494/)
- [Object Composition in Javascript](https://medium.com/code-monkey/object-composition-in-javascript-2f9b9077b5e6)
- [javascript-algorithms](https://github.com/trekhleb/javascript-algorithms)
- [Асинхронное программирование: концепция, реализация, примеры](https://proglib.io/p/asynchrony/)

### Browser
- [Рендеринг WEB-страницы: что об этом должен знать front-end разработчик](https://habr.com/post/224187/)

### Debugging
- [How to debug Front-end: Console](https://blog.pragmatists.com/how-to-debug-front-end-console-3456e4ee5504)
- [Консоль разработчика Google Chrome: десять неочевидных полезностей](https://habrahabr.ru/company/ruvds/blog/316132/)
- [Cool Chrome DevTools tips and tricks you wish you knew already](https://medium.freecodecamp.org/cool-chrome-devtools-tips-and-tricks-you-wish-you-knew-already-f54f65df88d2)

### Tests
- [An Overview of JavaScript Testing in 2018](https://medium.com/welldone-software/an-overview-of-javascript-testing-in-2018-f68950900bc3)

### Job
- [The Best Resources to Ace your Full Stack JavaScript Interview](https://medium.freecodecamp.org/5-top-sites-for-javascript-interview-preparation-71b48e9a6c8a)
- [The Definitive JavaScript Handbook for your next developer interview](https://medium.freecodecamp.org/the-definitive-javascript-handbook-for-a-developer-interview-44ffc6aeb54e)
- [10 JavaScript concepts you need to know for interviews](https://codeburst.io/10-javascript-concepts-you-need-to-know-for-interviews-136df65ecce)